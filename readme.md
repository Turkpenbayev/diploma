# Diploma 

**An complete Flask application template, with useful plugins.**

Use this Flask app to initiate your project with less work. In this application  template you will find the following plugins already configured:

* **Flask-Login** - Flask-Login provides user session management for Flask.
* **Flask-Bootstrap** - Ready-to-use Twitter-bootstrap for use in Flask.
* **Flask-Admin** - Flask extension module that provides an admin interface
* **Flask-SQLAlchemy** - Adds SQLAlchemy support to Flask. Quick and easy.
* **Flask-WTF** - Flask-Themes makes it easy for your application to support a wide range of appearances. And any view using **FlaskForm** to process the request is already getting CSRF protection.

## Requirements

Python 2.5+, python-pip, virtualenv

## Instalation

First, clone this repository:

    $ git clone https://gitlab.com/Turkpenbayev/diploma.git
    $ cd diploma

Install Pip on linux:

    $ sudo apt-get install python-pip

To install virtualenv globally with pip (if you have pip 1.3 or greater installed globally):

    $ sudo pip install virtualenv

Create a virtualenv, and activate this: 

    $ virtualenv env 
    $ source env/bin/activate

After, install all necessary to run:

    $ pip install -r requirements.txt

Than, run the application:

	$ python run.py

To see your application, access this url in your browser: 

	http://0.0.0.0:5000

All configuration is in: `configuration.py`

To change something:

	$ git checkout -b [name of your branch] without '[' and ']'

After changign:

	$ git add .
	$ git commit -m "What is changed, what did u do"
	$ git push