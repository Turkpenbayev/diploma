# -*- encoding: utf-8 -*-

from datetime import datetime

from flask import (flash, g, jsonify, redirect, render_template, request,
                   session, url_for)
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.base import MenuLink
from flask_admin.contrib.sqla import ModelView
from flask_login import (AnonymousUserMixin, LoginManager, current_user,
                         login_required, login_user, logout_user)

from werkzeug.security import check_password_hash, generate_password_hash

from app import app, db, login_manager

from .forms import LoginForm
from .models import Income_signals, Signal, User
from .smsApi import sendSms


class Anonymous(AnonymousUserMixin):
    def __init__(self):
        self.is_admin = False    


login_manager.anonymous_user = Anonymous

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                user_id=user.id
                login_user(user, remember=form.remember.data)
                if current_user.is_admin:
                    return redirect(url_for('admin.index'))
                return redirect(url_for('dashboard', user_id=user_id))
        message="Invalid username or password"
        return render_template('login.html', form=form, message=message)

    return render_template('login.html', form=form)

@app.route('/dashboard/<int:user_id>')
@login_required
def dashboard(user_id):
    if current_user.id!=user_id:
        return redirect(url_for('login'))
    name=current_user.name
    query="""SELECT id, signal_id, date, time, address, accepted 
            FROM INCOME_SIGNALS WHERE hospital_id="""+str(user_id)+" ORDER BY id DESC"

    data=db.session.execute(query).fetchall()
    len_data=len(data)

    return render_template('dashboard.html', name=name, user_id=user_id, data=data, len_data=len_data )



@app.route('/postjson/<int:signal_id>',methods=['GET', 'POST'])
def postJsonHandler(signal_id):
    if request.method=="GET":
        content = request.get_json()
        #signal_id = content['signal_id']
        try:
            query1="""SELECT hospital_id, address 
                    FROM SIGNAL WHERE id=""" + str(signal_id)

            data_query1=data=db.session.execute(query1).fetchone()
            hospital_id=int(data_query1['hospital_id'])
            address=str(data_query1['address'])
            date=datetime.today().date()
            time=datetime.today().time().strftime('%H:%M:%S')
            print(content)       

            new_signal=Income_signals(signal_id=signal_id, hospital_id=hospital_id, date=date, time=time,address=address)

            db.session.add(new_signal)
            db.session.commit()
            return 'JSON posted'
        except Exception as e:
            return 'Not such device'
    else:
        return "404"

@app.route('/update/<int:user_id>', methods=['GET', 'POST'])
@login_required
def update_data(user_id):
    user_id = request.args.get('user_id')
    len_data = request.args.get('length_data')
    query="""SELECT id, signal_id, date, time, address, accepted 
            FROM INCOME_SIGNALS WHERE hospital_id="""+str(user_id)+" ORDER BY id DESC"

    data=db.session.execute(query).fetchall()
    len_data_check=len(data)
    if str(len_data_check) == len_data:
        return jsonify('not update')
    else:
        context = []
        query="""SELECT id, signal_id, date, time, address, accepted 
            FROM INCOME_SIGNALS WHERE hospital_id="""+str(user_id)+" ORDER BY id DESC"

        data=Income_signals.query.filter_by(hospital_id = user_id)
        
        for d in data:
            context.append(d.toDict())

        return jsonify(context)



@app.route('/dashboard/<int:user_id>/<int:id>', methods=['GET', 'POST'])
@login_required
def dashboard_accepted(user_id, id):
    
    query="SELECT hospital_id, accepted, signal_id FROM INCOME_SIGNALS WHERE id="+str(id)
    queredData = db.session.execute(query).fetchone()

    if queredData[0]==user_id and queredData[1]==0:
        update="UPDATE income_signals SET accepted=1 WHERE id="+str(id)
        """
            Send signal to Arduino here!
        """
        phone = "+77071273935"
        message = "Go"
        r=sendSms(phones=phone,message=message)
        db.session.execute(update)
        db.session.commit()
        
        return jsonify({ 'accepted' : 1 })
    else:
        return jsonify({ 'accepted' : 0 })

@app.route('/report')
@login_required
def report():
    user_id = current_user.id
    return render_template('report.html',user_id=user_id)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

class UserView(ModelView):
    def is_accessible(self):
        return current_user.is_admin    
    
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))
        
    create_modal = True
    edit_modal = True

    def on_model_change(self, form, User, is_created):
        if form.password.data != ' ':
            User.set_password(form.password.data)
        else:
           del form.password

    def on_form_prefill(self, form, id):
        form.password.data = ''  

class AdminView(ModelView):
    def is_accessible(self):
        return current_user.is_admin    
    
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))

class HomeAdminView(AdminIndexView):
    @expose('/')
    def index(self):
        if current_user.id!=1:
            return redirect(url_for('login'))
        name=current_user.name
        query="""SELECT id, signal_id, date, time, address, accepted 
            FROM INCOME_SIGNALS"""

        data = db.session.execute(query).fetchall()

        return self.render('admin/index.html', data=data, name=name)

    def is_accessible(self):
        return current_user.is_admin
    
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('login'))



admin=Admin(app, "CardioM", url='/admin', index_view=HomeAdminView(name='Home'), template_mode='bootstrap3')
admin.add_link(MenuLink(name='Log out', url='/logout'))
admin.add_view(UserView(User,db.session))
admin.add_view(AdminView(Signal,db.session))
admin.add_view(AdminView(Income_signals,db.session))
